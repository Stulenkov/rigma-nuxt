export const scrollState = (selector, boolean) => {
    if(boolean){
        document.querySelector(selector).classList.add('no--scroll');
    }
    else{
        document.querySelector(selector).classList.remove('no--scroll');
    }
}