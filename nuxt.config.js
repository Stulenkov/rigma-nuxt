
export default {
  mode: 'universal',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  loading: { color: '#fff' },
  modules: [
    '@nuxtjs/svg-sprite',
    'vue-swatches/nuxt',
    'nuxt-vue-select',
    'nuxt-mq',
    'nuxt-user-agent',
  ],
  'mq': {
    defaultBreakpoint: 'sm',
    breakpoints: {
      mobile: 450,
      tablet: 1240,
      desktop: Infinity,
    }
  },
  css: ['@/assets/sass/main.sass'],
  buildModules: [
    '@nuxtjs/style-resources',
  ],
  styleResources: {
    sass: [
      '@/assets/sass/settings/config.sass',
      '@/assets/sass/mixins/responsive.sass',
    ]
  },
  svgSprite: {},
  plugins: [
    { src:'@/plugins/tui-editor', ssr: false},
    { src:'@/plugins/locomotiveScroll', ssr: false},
    { src:'@/plugins/vue-js-modal', ssr: false},
    { src:'@/plugins/swiper', ssr: false},
    { src:'@/plugins/number-input', ssr: false},
  ],
  build: {
    vendor: ['@toast-ui/vue-editor'],
  }
}
