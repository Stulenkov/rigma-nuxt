## Установка

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

## Страницы

### Лендос
* [Главная](http://rigma.now.sh)
* [Страница с ценами](http://rigma.now.sh/tarifs)

### Конструктор
* [Регистрация](http://rigma.now.sh/registration)
* [Отправная точка](http://rigma.now.sh/starting_point)
* [Настройки темы](http://rigma.now.sh/theme_settings)
* [Дизайн](http://rigma.now.sh/design)
* [Контент](http://rigma.now.sh/content)
* [Настройки корзины](http://rigma.now.sh/bucket_settings)
* [Настройки (тут пример главной страницы в фрейме)](http://rigma.now.sh/settings)
* [Настройки SEO (тут пример страницы товара в фрейме)](http://rigma.now.sh/seo_settings)


### Шаблон 1
* [Главная](http://rigma.now.sh/constructor_main)
* [Главная (разные типы карточек и первого экрана)](http://rigma.now.sh/constructor_main-2)
* [Главная (разные типы карточек и первого экрана)](http://rigma.now.sh/constructor_main-3)
* [Главная (разные типы карточек и первого экрана)](http://rigma.now.sh/constructor_main-4)
* [Главная (разные типы карточек и первого экрана)](http://rigma.now.sh/constructor_main-5)
* [Страница товара](http://rigma.now.sh/constructor_product)


## Немного о структуре данных для главной страницы (Шаблон 1)
### Шапка (header)
* `logo` - Текст логотипа
* `menu` - Список пунктов меню
* `contacts` - Контакты

### Главный экран (hero)
* `title` - Заголовок
* `subtitle` - Подзаголовок
* `background` - Картинка на фон
* `type` - Тип отображения. Возможные значения - `full`, `half`
* `contentPosition` - Расположение контента. Возможные значения - `bottom`, `middle`, `center`

### Каталог и карточки товара (catalog)
* `columns` - Число колонок списка с товарами
* `cardView` - Вид товара. Возможные значения - `one`, `two`, `three`, `four`, `five`
* `list` - Список товаров