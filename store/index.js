import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

const store = () =>
  new Vuex.Store({
    state: {
      url: "https://www.mocky.io/v2/",
      constructor: {
        media: "desktop"
      }
    },
    getters: {
      urlAPI: state => {
        return state.url + "/json/";
      }
    },
  });

export default store;